package sandwich;

public class NeuralDriver extends Controller {
	/* DEBUG mode toggle */
	private boolean DEBUG = false;

	/* Gear changing constants */
	final int[] gearUp = {9000,9000,9000,9000,9000,0};
	final int[] gearDown = {0,3800,5500,6000,6000,6000};

	/* Stuck constants */
	final int stuckTime = 30;
	final float stuckAngle = 0.523598775f; // PI/6

	/* Steering constants */
	final float steerLock = 0.366519f;

	/* Anti Blocking System (ABS) Filter Constants */
	final float wheelRadius[] = {0.3306f, 0.3306f, 0.3276f, 0.3276f};
	final float absSlip = 2.0f;
	final float absRange = 3.0f;
	final float absMinSpeed = 2.5f;

	/* Traction Control System (TCS) Constants */
	final float tcsSlip = 1.0f;
	final float tcsRange = 1.0f;

	/* Clutching Constants */
	final float clutchMax = 0.5f;
	final float clutchDelta = 0.05f;
	final float clutchRange = 0.82f;
	final float clutchDeltaTime = 0.01f;
	final float clutchDeltaRaced = 10f;
	final float clutchDec = 0.01f;
	final float clutchMaxModifier = 1.3f;
	final float clutchMaxTime = 1.4f;

	/* Local fields */
	private NeuralNetwork neuralNetwork;// Neural network
	private float clutch = 0;			// Current clutch
	private int stuck = 0;				// Stuck counter
	private boolean isStuck = false;	// Stuck flag

	/**
	 * Constructor
	 * <p>
	 * Used to setup the neural driver and neural network.
	 * <p>
	 * newNetwork states if a new network should be crated or not.
	 * training states if the network should be trained or not.
	 */
	public NeuralDriver() {
		boolean newNetwork	= false; // Create new network
		boolean training	= false; // Train network

		if (newNetwork) {
			// NeuralNetwork(input, hidden1, hidden2, output)
			neuralNetwork = new NeuralNetwork(22, 19, 0, 3); // Create new
		} else {
			neuralNetwork = new NeuralNetwork(); // Create without BasicNetwork

			// Load existing BasicNetwork from file
			neuralNetwork.loadGenome("./memory/memory.mem");
		}

		if (training) {
			neuralNetwork.train(7000, "../training/normalized/combined_data.csv");

			// Store BasicNetwork after training
			neuralNetwork.storeGenome("./memory/custom.mem");
		}

		System.out.println("Ready to race!");
	}

	public void reset() { System.out.println("Restarting"); }

	public void shutdown() {
		System.out.println("OOF!");
		neuralNetwork.kill();
	}

	/**
	 * Driver control
	 *
	 * @param sensors SensorModel object contraining Torcs sensors
	 * @return Action object
	 */
	public Action control(SensorModel sensors){
		/* Check if car is currently stuck */
		if ((Math.abs(sensors.getAngleToTrackAxis()) > stuckAngle)
			|| (sensors.getTrackEdgeSensors()[9] == -1.0))
			stuck++; // Update stuck counter
		else
			stuck = 0;

		/* If car is stuck for a while apply recovering policy */
		if (stuck > stuckTime || isStuck) {
			if (!isStuck) isStuck = true;
			return recover(sensors);
		}

		/* Use neural network if car is not stuck */
		double[] output = neuralNetwork.getOutput(sensors);

		/* Setup our next action */
		clutch = clutching(sensors, clutch);
		double accel = filterTCS(sensors, output[0]); // Traction Control System
		double brake = output[1];

		if (brake > 0 && accel < 0.6) // Only brake when needed
			brake = filterABS(sensors, brake); // Anti Blocking System
		else
			brake = 0;

		/* Next neural network action */
		Action action		= new Action();
		action.gear			= getGear(sensors);
		action.accelerate	= accel;
		action.brake		= brake;
		action.steering		= output[2];
		action.clutch		= clutch;

		if (DEBUG) printValues(action); // Print action values in console

		return action;
	}

	/**
	 * Anti-lock Braking System (ABS)
	 * <p>
	 * Prevents wheels from slipping when braking hard.
	 *
	 * @param sensors SensorModel object contraining Torcs sensors
	 * @param brake requested brake value
	 * @return compensated brake value
	 */
	private double filterABS(SensorModel sensors, double brake){
		/* Convert speed to m/s */
		double speed = (double) (sensors.getSpeed() / 3.6);

		/* Do nothing at low speeds */
		if (speed < absMinSpeed) return (double) brake;

		/* Compute the speed of wheels in m/s */
		double slip = (float) 0.0;
		for (int i = 0; i < 4; i++)
			slip += sensors.getWheelSpinVelocity()[i] * wheelRadius[i];

		/* Slip is difference between actual speed of car
			and average speed of wheels */
		slip = speed - slip / 4.0f;

		if (slip > absSlip) // If slip is too high apply ABS
			brake = brake - (slip - absSlip) / absRange;

		if (brake < 0) return 0; // ABS Release brake

		return (double) brake; // Else keep Braking
	}

	/**
	 * Traction Control System (TCS)
	 * <p>
	 * Prevents wheels from slipping when accelerating fast.
	 *
	 * @param sensors SensorModel object contraining Torcs sensors
	 * @param accel requested acceleration value
	 * @return compensated acceleration value
	 */
	private double filterTCS(SensorModel sensors, double accel){
		/* Convert speed to m/s */
		float speed = (float) (sensors.getSpeed() / 3.6);

		/* Compute the speed of wheels in m/s */
		float slip = 0.0f;
		for (int i = 0; i < 4; i++)
			slip += sensors.getWheelSpinVelocity()[i] * wheelRadius[i];

		/* Slip is difference between actual speed of car
			and average speed of wheels */
		slip = speed - slip / 4.0f;

		if (-slip > tcsSlip) // when slip too high apply ASR
			accel = accel + (slip + tcsSlip) / tcsRange;

		if (accel < 0) return 0; // TCS no acceleration

		if (accel > 0.9999) return 1; // TCS full acceleration

		return accel; // Else keep accelerating
	}

	/**
	 * Automatic clutching
	 *
	 * @param sensors SensorModel object contraining Torcs sensors
	 * @param clutch current clutch value
	 * @return calculated clutch value
	 */
	private float clutching(SensorModel sensors, float clutch) {
		float maxClutch = clutchMax;

		/* Check if the current situation is the race start */
		if ((sensors.getCurrentLapTime() < clutchDeltaTime)
			&& (getStage() == Stage.RACE)
			&& (sensors.getDistanceRaced() < clutchDeltaRaced)) {
				clutch = maxClutch;
		}

		/* Adjust the current value of the clutch */
		if (clutch > 0) {
			double delta = clutchDelta;
			if (sensors.getGear() < 1) {
				/* Apply stronger clutch when the race is just started */
				delta /= 2;
				maxClutch *= clutchMaxModifier;
				if (sensors.getCurrentLapTime() < clutchMaxTime)
					clutch = maxClutch;
			}

			/* Check clutch is not bigger than maximum values */
			clutch = Math.min(maxClutch, clutch);

			/* If clutch is not at max value decrease it quite quickly */
			if (clutch != maxClutch) {
				clutch -= delta;
				clutch = Math.max((float) 0.0, clutch);
			} else {
				clutch -= clutchDec; // Else decrease it very slowly
			}
		}
		return clutch;
	}

	/**
	 * Automatic gearbox
	 * <p>
	 * Automatically change gear acording to the current RPM.
	 *
	 * @param sensors SensorModel object contraining Torcs sensors
	 * @return prefered gear for current RPM
	 */
	private int getGear(SensorModel sensors){
		int gear = sensors.getGear();
		double rpm = sensors.getRPM();

		if (gear<1) return 1; // If gear is 0 (N) or -1 (R) just return 1

		/* Check if the RPM value of car is greater than the one suggested */
		if (gear < 6 && rpm >= gearUp[gear-1])
			return gear + 1;

		/* Check if the RPM value of car is lower than the one suggested */
		if (gear > 1 && rpm <= gearDown[gear-1])
			return gear - 1;

		return gear; // Otherwise keep current gear
	}

	/**
	 * Automatic recover
	 * <p>
	 * If the car is stuck, make the car face the right direction and
	 * align the car with the track axis.
	 *
	 * @param sensors SensorModel object contraining Torcs sensors
	 * @return Action object
	 */
	private Action recover(SensorModel sensors) {
		/* Recover values */
		double targetAngle = sensors.getAngleToTrackAxis()-sensors.getTrackPosition()*0.5;
		double brake = 0.0;
		double accel = filterTCS(sensors, 0.8);
		double steer = 0.0;
		int gear = 0;

		/* if car is pointing in the correct direction, move forwards */
		if (sensors.getAngleToTrackAxis() * sensors.getTrackPosition() > -0.2) {
			if (sensors.getSpeed() < -0.1) {
				/* Moving backwards, make the car stop */
				accel = 0.0;
				brake = filterABS(sensors, 1.0);
			} else {
				/* Drive towards the center of the track */
				accel = filterTCS(sensors, 1.0);
				brake = 0.0;
				gear = 1;

				/* Change steering angle depending on speed */
				if (sensors.getSpeed() > 80.0)
					steer = targetAngle/(steerLock*(sensors.getSpeed()-80.0));
				else
					steer = (targetAngle)/steerLock;

				/* Check if we are roughly in the middle if so,
					neural network can take over car control */
				if (Math.abs(targetAngle-sensors.getAngleToTrackAxis()) < 0.008)
					isStuck = false;
			}
		}
		/* if we are not facing in the correct direction, reverse */
		else {
			if (sensors.getSpeed() > 20.0) {
				/* Moving forwards, make the car stop */
				accel = 0.0;
				brake = filterABS(sensors, 1.0);
			} else {
				/* Move backwards and steer towards the center of the track */
				accel = filterTCS(sensors, 1.0);
				brake = 0.0;
				gear = -1;

				/* Change steering angle depending on speed */
				if (sensors.getSpeed() > 80.0)
					steer = -targetAngle/(steerLock*(sensors.getSpeed()-80.0));
				else
					steer = -(targetAngle)/steerLock;
			}
		}

		/* Keep steering values within range */
		if (steer < -1.0) steer = -1.0;
		if (steer > 1.0) steer = 1.0;

		/* Return auto recovering action */
		Action action		= new Action();
		action.gear			= gear;
		action.steering		= steer;
		action.accelerate	= accel;
		action.brake		= brake;
		action.clutch		= clutching(sensors, clutch);
		return action;
	}

	/**
	 * Print steering, acceleration and braking values as graphs
	 * <p>
	 * Note: This will clear the screen each time it is executed,
	 * not recommended during racing competitions!
	 *
	 * @param a Action object contraining valid action values
	 */
	public void printValues(Action a) {
		System.out.print("\033[H\033[2J");
		System.out.flush();

		int steer = (int) Math.round(a.steering * 7);
		int accel = (int) Math.round(a.accelerate * 10);
		int brake = (int) Math.round(a.brake * 10);

		System.out.print("Steer:\t");
		switch (steer) {
			case -7:	System.out.print("--------------|"); break;
			case -6:	System.out.print("-------------|-"); break;
			case -5:	System.out.print("------------|--"); break;
			case -4:	System.out.print("-----------|---"); break;
			case -3:	System.out.print("----------|----"); break;
			case -2:	System.out.print("---------|-----"); break;
			case -1:	System.out.print("--------|------"); break;
			case 0:		System.out.print("-------|-------"); break;
			case 1:		System.out.print("------|--------"); break;
			case 2:		System.out.print("-----|---------"); break;
			case 3:		System.out.print("----|----------"); break;
			case 4:		System.out.print("---|-----------"); break;
			case 5:		System.out.print("--|------------"); break;
			case 6:		System.out.print("-|-------------"); break;
			case 7:		System.out.print("|--------------"); break;
		}
		System.out.println();
		System.out.print("Accel:\t");
		switch (accel) {
			case 0:		System.out.print("          "); break;
			case 1:		System.out.print("#         "); break;
			case 2:		System.out.print("##        "); break;
			case 3:		System.out.print("###       "); break;
			case 4:		System.out.print("####      "); break;
			case 5:		System.out.print("#####     "); break;
			case 6:		System.out.print("######    "); break;
			case 7:		System.out.print("#######   "); break;
			case 8:		System.out.print("########  "); break;
			case 9:		System.out.print("######### "); break;
			case 10:	System.out.print("##########"); break;
		}
		System.out.println();
		System.out.print("Brake:\t");
		switch (brake) {
			case 0:		System.out.print("          "); break;
			case 1:		System.out.print("#         "); break;
			case 2:		System.out.print("##        "); break;
			case 3:		System.out.print("###       "); break;
			case 4:		System.out.print("####      "); break;
			case 5:		System.out.print("#####     "); break;
			case 6:		System.out.print("######    "); break;
			case 7:		System.out.print("#######   "); break;
			case 8:		System.out.print("########  "); break;
			case 9:		System.out.print("######### "); break;
			case 10:	System.out.print("##########"); break;
		}
		System.out.println();
	}

	/* Setup the angles of the TrackEdgeSensors */
	public float[] initAngles() {
		/* from left (-) to right (+), where 0 is forwards */
		float[] angles = {-90,-80,-70,-60,-50,-40,-30,-20,-10,0,10,20,30,40,50,60,70,80,90};
		return angles;
	}
}
