#!/usr/bin/env python3

import sys
import csv

def normalise_function(num, d_low, d_high, n_high=1, n_low=-1):
    """Function to normalise num with range d, to range n"""
    if d_low == 0 and d_high == 0: return num

    return (num - d_low)*(n_high - n_low)/(d_high - d_low) + n_low

def normilise_rows(rows, norm_rows):

    new_rows = []

    # print('NEW MATRIX')

    for row in rows:
        new_row = []
        for num_index, num in enumerate(row):
            new_row += [normalise_function(
                num, norm_rows[num_index][0],
                norm_rows[num_index][1])]
        # print(new_row)
        new_rows += [new_row]
    return new_rows

def get_rows(file_name):

    reader = csv.reader(open(file_name))

    rows = []
    read_header = True
    for row in reader:
        if read_header:
            read_header = False
        else:
            row = list(map(float, row))
            rows += [row]
        # print(row)
    # print()
    return rows

def write_csv(file_name_new, rows):
    csvfile = open(file_name_new, 'w')
    writer = csv.writer(csvfile)
    writer.writerows(rows)

def print_help():
    message = 'USEAGE: ./csv_manipulator <file> <new_file> <normalise file>'
    message += '\ncsv files need HEADERS'
    message += '\nTo ignore normalization, norm high and low should both be 0'
    print(message)
    exit()

def main():
    if len(sys.argv) < 2: print_help()

    file_name = sys.argv[1]
    file_name_new = sys.argv[2]
    normalise_file = sys.argv[3]

    rows = get_rows(file_name)
    norm_rows = get_rows(normalise_file)

    rows_normalised = normilise_rows(rows, norm_rows)
    write_csv(file_name_new, rows_normalised)

if __name__ == '__main__':
    main()
